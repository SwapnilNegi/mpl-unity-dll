﻿
public class PhotonConstants 
{
    public static string APP_ID = "6e58a79c-0872-4660-a5f5-e47961a92b0b";
    public static string APP_VERSION = "Tiny_Militia";
    public static string APP_REGION = "in";

    public static bool RunInBackground = true;

    public static float KeepAliveInBackground = 0;

    public static int MAX_PLAYERS = 2;

}
