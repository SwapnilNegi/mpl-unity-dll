﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;

public class ReceiveEventHandler : MonoBehaviourPunCallbacks, IOnEventCallback
{
    public static ReceiveEventHandler instance;
   
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
    }
    private void Start()
    {
       // DontDestroyOnLoad(gameObject);
    }
    public void Init()
    {
        
    }

    private void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    private void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }
    public const byte ownTurnSentEvent = 101;
    public const byte oppoTurnSentEvent = 102;
    public const byte pingDataMaster = 103;
    public const byte pingDataOther = 104;
    public const byte scoreUpdate = 105;
    public const byte ownHealthUpdate = 106;
    public const byte oppoHealthUpdate = 107;
    public const byte freezeTimeEvent = 108;
    public const byte ownEvent = 109;
    public const byte oppoEvent = 110;
    public const byte pauseEvent = 111;
    public const byte logicDisconnectEvent = 112;

    int count = 0;

    TaskCompletedCallBack taskCompletedCallBack;
    public void StartNewTask(TaskCompletedCallBack taskCompletedCallBack)
    {
        if (taskCompletedCallBack != null)
            taskCompletedCallBack("I have completed Task.");
    }
    public void OwnTurnSentEvent()
    {
        object[] content = new object[] {PhotonNetwork.Time};
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(ownTurnSentEvent, content, raiseEvent,SendOptions.SendReliable);
        count++;
        Debug.LogError("Count 2nd :" + count);
    }

    public void OppoTurnSentEvent()
    {
        object[] content = new object[] { PhotonNetwork.Time };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(oppoTurnSentEvent, content, raiseEvent, SendOptions.SendReliable);
    }

    public void SendPingDataMaster(int pValues)
    {
        object[] content = new object[] { pValues };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(pingDataMaster, content, raiseEvent, SendOptions.SendReliable);
    }
    public void SendPingDataMOther(int pValues)
    {
        object[] content = new object[] { pValues };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(pingDataOther, content, raiseEvent, SendOptions.SendReliable);
    }
    public void ScoreUpdate(int pValues, int pKey)
    {
        object[] content = new object[] { pValues, pKey };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(scoreUpdate, content, raiseEvent, SendOptions.SendReliable);
    }

    public void DeductScoreOwn(int pValues, int pKey)
    {
        object[] content = new object[] { pValues, pKey };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(ownHealthUpdate, content, raiseEvent, SendOptions.SendReliable);
    }

    public void DeductScoreOppo(int pValues, int pKey)
    {
        object[] content = new object[] { pValues, pKey };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(oppoHealthUpdate, content, raiseEvent, SendOptions.SendReliable);
    }

    public void FreezeTimeEvent()
    {
        object[] content = new object[] { true };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.All };
        PhotonNetwork.RaiseEvent(freezeTimeEvent, content, raiseEvent, SendOptions.SendReliable);
    }

    public void SendByOwnEvent(float time)
    {
        object[] content = new object[] { time };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(ownEvent, content, raiseEvent, SendOptions.SendReliable);
   
    }
    public void SendByOpoEvent(float time)
    {
        object[] content = new object[] { time };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(oppoEvent, content, raiseEvent, SendOptions.SendReliable);
      
    }

    public void SendPauseEvent(bool states)
    {
        object[] content = new object[] { states };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(pauseEvent, content, raiseEvent, SendOptions.SendReliable);

    }
    public void SendLogicDisconnectEvent()
    {
        object[] content = new object[] { true };
        RaiseEventOptions raiseEvent = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
        PhotonNetwork.RaiseEvent(logicDisconnectEvent, content, raiseEvent, SendOptions.SendReliable);

    }


    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;
        if(eventCode == ownTurnSentEvent)
        {
            object[] data = (object[])photonEvent.CustomData;
            double OwnTurnEvent = (double)data[0];
            ReconnectionHandler.Instance.OwnTurnReceiverEvent((float)OwnTurnEvent);
        }
        if (eventCode == oppoTurnSentEvent)
        {
            object[] data = (object[])photonEvent.CustomData;
            double OppTurnEvent = (double)data[0];
            ReconnectionHandler.Instance.OppoTurnReceiverEvent((float)OppTurnEvent);
        }
        if (eventCode == pingDataMaster)
        {
            object[] data = (object[])photonEvent.CustomData;
            int pingData = (int)data[0];
            ReconnectionHandler.Instance.RecievePing(pingData);
        }
        if (eventCode == pingDataOther)
        {
            object[] data = (object[])photonEvent.CustomData;
            int pingData = (int)data[0];
            ReconnectionHandler.Instance.RecievePing(pingData);
        }
        if (eventCode == scoreUpdate)
        {
            object[] data = (object[])photonEvent.CustomData;
            int score = (int)data[0];
            int keys = (int)data[1];
            ReconnectionHandler.Instance.ScoreUpdate(score,keys);
        }
        if (eventCode == ownHealthUpdate)
        {
            object[] data = (object[])photonEvent.CustomData;
            int score = (int)data[0];
            int keys = (int)data[1];
            ReconnectionHandler.Instance.ScoreHealthUpdateResponse(score,0, keys);
        }
        if (eventCode == oppoHealthUpdate)
        {
            object[] data = (object[])photonEvent.CustomData;
            int score = (int)data[0];
            int keys = (int)data[1];
            ReconnectionHandler.Instance.ScoreHealthUpdateResponse(0,score, keys);
        }
        if (eventCode == ownEvent)
        {
            object[] data = (object[])photonEvent.CustomData;
            float time = (float)data[0];
            ReconnectionHandler.Instance.CallBackRecieveUserOppo(time);

        }
        if (eventCode == oppoEvent)
        {
            object[] data = (object[])photonEvent.CustomData;
            float time = (float)data[0];
            ReconnectionHandler.Instance.CallBackRecieveFromOwn(time);
  
        }
        if (eventCode == pauseEvent)
        {
            object[] data = (object[])photonEvent.CustomData;
            bool pause = (bool)data[0];
            ReconnectionHandler.Instance.PauseEventCallBack(pause);

        }

        if (eventCode == logicDisconnectEvent)
        {
            object[] data = (object[])photonEvent.CustomData;
            bool pause = (bool)data[0];
            ReconnectionHandler.Instance.DisconnectedbyCleintLogic();

        }

    }
    public static ReceiveEventHandler Instance
    {
        get
        {
            //if(instance == null)
            //{
            //    GameObject game = new GameObject();
            //    game.name = "ReceiveEventHandler";
            //    game.AddComponent<ReceiveEventHandler>();

            //}
            return instance;
        }
    }
  
}
