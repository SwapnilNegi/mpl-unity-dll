﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
//using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Manages the initial connection and matchmaking
/// </summary>

public class PhotonManager : MonoBehaviourPunCallbacks
{
    #region Delegates
    public delegate void OnMatchmakingComplete(Dictionary<int, Player> users);
    public event OnMatchmakingComplete OnMatchmakingCompleted;

    public delegate void OnPhotonDisconnected();
    public event OnPhotonDisconnected OnDisconnectedPhoton;

    public delegate void OnPhotonStateChanged(PhotonConnectionStatus status);
    public event OnPhotonStateChanged OnPhotonStatusChanged;

    public delegate void OnAllPlayersLeft();
    public static OnAllPlayersLeft OnAllPlayersLeftRoom;
    #endregion

    // Internal variables:

    public static int totalPlayerCount;
    public List<Player> playersinRoom = new List<Player>();

    private int MAX_PLAYERS = PhotonConstants.MAX_PLAYERS;
    private string currentRoomName = "";

    [SerializeField] float waitingForPlayers = 20;//Time to wait for players to join
    [SerializeField] float connectionTimeout;//Time after which the matchmaking will happen again

    public string CurrentRoom
    {
        get { return currentRoomName; }
    }

    private PhotonConnectionStatus status = PhotonConnectionStatus.Disconnected;
    public PhotonConnectionStatus PhotonStatus
    {
        get { return status; }
        set
        {
            status = value;
            OnPhotonStatusChanged.Invoke(status);
        }
    }

    public static PhotonManager Instance;

    private IEnumerator submitResultCoroutine;
    private bool matchStarted = false;
    private bool connectionInitiated = false;
    private bool connectionTimedOut = false;

    #region Unity Callbacks
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);

        Debug.Log("<color=green>Photon Manager Awake() called</color>");

        //Set PhotonConnectSettings using config
        PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = PhotonConstants.APP_ID;
        PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = PhotonConstants.APP_VERSION;
        PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = PhotonConstants.APP_REGION;

        PhotonNetwork.PhotonServerSettings.RunInBackground = PhotonConstants.RunInBackground;
        PhotonNetwork.KeepAliveInBackground = PhotonConstants.KeepAliveInBackground;
    }

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    //public bool test = false;
    // Update is called once per frame
    void Update()
    {
        // Room managing:
        if (PhotonNetwork.InRoom)
        {
            //Debug.Log("<color=green>MPL SDK : PhotonPlayersInRoom - </color>" + PhotonNetwork.CurrentRoom.PlayerCount + " : " + PhotonNetwork.CurrentRoom.ToStringFull() + PhotonNetwork.CurrentRoom.IsOpen);
            // Go to the game scene when the room is already full:

            waitingForPlayers -= Time.deltaTime;

            if ((PhotonNetwork.CurrentRoom.PlayerCount == MAX_PLAYERS || waitingForPlayers <= 0) && status != PhotonConnectionStatus.ReadyToPlay)
            {
                Debug.Log("Am I the master client:" + PhotonNetwork.IsMasterClient);

                if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
                {
                    //if we are the only one in the room after a timeout go back to matchmaking
                    MultiPlayerCanvasController1vN.Instance.matchMakingController1VN.OnRoomJoinFailed(SmartFoxManager.SmartFoxManagerReasons.OpponentDidNotJoin);
                    CancelMatchmaking();
                    return;
                }

                PhotonNetwork.CurrentRoom.IsOpen = false;

                Debug.Log("<color=green>MPL SDK : PhotonPlayersInRoom - </color>" + PhotonNetwork.CurrentRoom.PlayerCount + " : " + PhotonNetwork.CurrentRoom.ToStringFull() + PhotonNetwork.CurrentRoom.IsOpen);

                status = PhotonConnectionStatus.ReadyToPlay;

                //Call game flow
                //SmartFoxManager.Instance.ReadyToPlay();
                Debug.Log("<color=green>MPL SDK: Photon Ready to Play</color>");

                OnMatchmakingCompleted?.Invoke(PhotonNetwork.CurrentRoom.Players);

                matchStarted = true;

                //Load the game scene if you are master client.
                if (PhotonNetwork.IsMasterClient && SceneManager.GetActiveScene().name != MPLController.Instance.sceneName)
                    PhotonNetwork.LoadLevel(MPLController.Instance.sceneName);
            }
        }
        else
        {
            if (status == PhotonConnectionStatus.Disconnected && connectionInitiated)
            {
                connectionTimeout -= Time.deltaTime;
                //Debug.LogError("Inside Connection TimeOut: " + connectionTimeout);
                if (connectionTimeout <= 0 && !connectionTimedOut)
                {
                    connectionTimedOut = true;
                    Debug.Log("PhotonManager :: Connection Retry Timeout");
                    SmartFoxManager.Instance.SetConnectionFlags("Photon Connection Timeout");
                    MultiPlayerCanvasController1vN.Instance.Disconnect(true, MPLGameEndReason.GameEndReasons.CONNECTION_RETRY_TIMEOUT);
                }
            }
        }
    }
    #endregion
    #region Class Methods
    void Reconnect()
    {
        Debug.Log("<color=green>MPL SDK: Reconnecting to photon servers</color>");
        StartConnection();
    }

    /// <summary>
    /// Initiates a connection with photon servers using the connect settings.
    /// </summary>
    public void StartConnection()
    {
        Debug.Log("<color=green>MPL SDK: Connecting to photon servers</color>");

        connectionInitiated = true;
        matchStarted = false;
        MAX_PLAYERS = MultiPlayerCanvasController1vN.Instance.GetUserCount();
        ResetConnectionTimeout();

        PlayerPrefs.SetInt("MaxPlayers", MultiPlayerCanvasController1vN.Instance.GetUserCount());

        if ((status == PhotonConnectionStatus.Disconnected) && !PhotonNetwork.IsConnected)
        {
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    private void ResetConnectionTimeout()
    {
        status = PhotonConnectionStatus.Disconnected;
        waitingForPlayers = 20;
        connectionTimeout = waitingForPlayers + 5;
        connectionTimedOut = false;
    }

    public void Disconnect()
    {
        Debug.Log("<color=green>MPL SDK: PhotonManager :: Disconnect()</color>");
        PhotonNetwork.Disconnect();
    }

    /// <summary>
    /// Creates a room if you are the owner otherwise join it.
    /// </summary>
    public void FindMatch()
    {
        if (status != PhotonConnectionStatus.Connected)
        {
            Debug.Log("<color=green>MPL SDK: Not connected to photon, cant find a room</color>");
            return;
        }

        if (MPLController.Instance == null || MPLController.Instance.battleRoomEventProperties == null)
        {
            Debug.Log("<color=green>MPL SDK: MPLController is null</color>");
            return;
        }          

        Debug.Log("<color=green>MPL SDK: Photon FindMatch called </color>" + MPLController.Instance.battleRoomEventProperties.BattleId + " : " + MPLController.Instance.battleRoomEventProperties.GameName);

        currentRoomName = "MM_" + MPLController.Instance.battleRoomEventProperties.BattleId;

        //Room Options
        RoomOptions roomOptions = new RoomOptions()
        {
            PlayerTtl = 15000,
            EmptyRoomTtl = 15000,
            MaxPlayers = (byte)MAX_PLAYERS
        };

        if (SmartFoxManager.Instance.isRoomOwner)
            PhotonNetwork.CreateRoom(currentRoomName, roomOptions, TypedLobby.Default);
        else
            PhotonNetwork.JoinRoom(currentRoomName);

        //Session.Instance.Restart += Reconnect;
    }

    public void CancelMatchmaking()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.Disconnect();
    }
    #endregion

    #region Photon Callbacks
    /// <summary>
    /// Called when connected to master server. We are now ready to join a room.
    /// </summary>
    public override void OnConnectedToMaster()
    {
        status = PhotonConnectionStatus.Connected;
        Debug.Log("<color=green>MPL SDK: Connected to Photon Server!</color>");

        connectionInitiated = false;

        FindMatch();
    }

    /// <summary>
    /// Called after disconnecting from the Photon server.
    /// </summary>
    /// <param name="cause"></param>
    public override void OnDisconnected(DisconnectCause cause)
    {
        status = PhotonConnectionStatus.Disconnected;

        ResetConnectionTimeout();

        Debug.Log("<color=green>MPL SDK: PhotonManager :: Disconnected from Photon Server!</color>");
        OnDisconnectedPhoton?.Invoke();

        //Session.Instance.Restart -= Reconnect;

        //TODO: This has been added to load test scene so that we can reload using PhotonNetwork.LoadLevel.Uncomment after final changes.
        if (cause == DisconnectCause.DisconnectByClientLogic && matchStarted)
        {
            SceneManager.LoadScene(1);
            Session.Instance.RestartSession();

            //Reset retries for new session.
            PhotonDisconnectionHandler.Instance.RetryCount = 2;
        }          

        //if (cause != DisconnectCause.DisconnectByClientLogic)
        //    MPL.utility.toastMessage.Toast.instance?.ShowMessage("Network lost. Trying to reconnect...", 10);
    }

    void OnPhotonPlayerConnected(Photon.Realtime.Player player)
    {
        // When a player connects, update the player count:
    }

    void OnPhotonPlayerDisconnected(Photon.Realtime.Player otherPlayer)
    {
        // When a player disconnects, update the player count:

        // Also, if a player disconnects while matchmaking and they happen to be the master client, a new master client will be assigned.
        // We could be the new master client so check if we are. If we are, continue adding bots (if bots are allowed):
        if (PhotonNetwork.IsMasterClient)
        {
            // Get the existing bot list made by the last master client, or make a new one if none:
            //curBots = GetBotList();
            // Start creating bots after a delay:
            //Invoke("StartCreatingBots", curBots.Length > 0 ? Random.Range(minBotCreationTime, maxBotCreationTime) : startCreatingBotsAfter);
        }
    }

    void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        Debug.Log("OnPhotonCustomRoomPropertiesChanged - maybe a bot have joined");
        // A bot might have joined, so update the total player count:
    }

    void OnPhotonRandomJoinFailed()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = (byte)MAX_PLAYERS }, null);
        }
    }

    void OnJoinRoomFailed()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    /// <summary>
    /// Called when entering a room (by creating or joining it). Called on all clients (including the Master Client).
    /// </summary>
    public override void OnJoinedRoom()
    {
        Debug.Log("<color=green>MPL SDK: OnJoinedRoom</color>");
        Debug.Log("<color=green>MPL SDK : PhotonPlayersInRoom - </color>" + PhotonNetwork.CurrentRoom.PlayerCount + " : " + PhotonNetwork.CurrentRoom.ToStringFull() + PhotonNetwork.CurrentRoom.IsOpen);
        // This is only used to check if we've loaded the game and ready.
    }

    /// <summary>
    /// Called when a CreateRoom() call failed. The parameter provides ErrorCode and message (as array).
    /// </summary>
    /// <param name="returnCode"></param>
    /// <param name="message"></param>
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        currentRoomName = "MM_" + MPLController.Instance.battleRoomEventProperties.BattleId;
        //Room Options
        RoomOptions roomOptions = new RoomOptions()
        {
            PlayerTtl = 10000,
            EmptyRoomTtl = 10000,
            MaxPlayers = (byte)MAX_PLAYERS,
            CleanupCacheOnLeave = false
        };
        PhotonNetwork.CreateRoom(currentRoomName, roomOptions, TypedLobby.Default);
    }

    /// <summary>
    /// Called when a JoinRoom() call failed. The parameter provides ErrorCode and message (as array).
    /// </summary>
    /// <param name="returnCode"></param>
    /// <param name="message"></param>
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        currentRoomName = "MM_" + MPLController.Instance.battleRoomEventProperties.BattleId;
        PhotonNetwork.JoinRoom(currentRoomName);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log("<color=green > MPL SDK: PhotonManager :: OnPlayerEnteredRoom</color>" + newPlayer.NickName);

        if(submitResultCoroutine != null)
        {

        }
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        //Only check if a player was not able to reconnect.
        if (status != PhotonConnectionStatus.ReadyToPlay || !otherPlayer.IsInactive)
            return;

        //Only start the wait time after all the players become inactive.
        Debug.Log("<color=green>MPL SDK: PhotonManager :: OnPlayerLeftRoom</color>" + otherPlayer.NickName);

        //MPL.utility.toastMessage.Toast.instance?.ShowMessage("Waiting for opponent to rejoin.", 10);

        int remainingPlayersInRoom = PhotonNetwork.PlayerList.Length;

        Debug.Log("MPL SDK: OnPlayerLeftRoom() Remaining players count :" + remainingPlayersInRoom);

        //If we are the only player in the room and other players are inactive for more than player TTL then submit your score.
        if (remainingPlayersInRoom == 1)
        {
            if (submitResultCoroutine != null)
            {
                StopCoroutine(submitResultCoroutine);
            }

            Debug.Log("MPL SDK: We are the only player left in the room so submitting results");
            submitResultCoroutine = SubmitResultIfAllPlayersHaveLeft();
            StartCoroutine(submitResultCoroutine);
        }
    }

    IEnumerator SubmitResultIfAllPlayersHaveLeft()
    {
        int waitTime = 2; //Wait for some extra time to cater networks lags.

        while (waitTime >= 0)
        {
            yield return new WaitForSeconds(1f);
            waitTime--;
        }

        Debug.Log("MPL SDK: SubmitResultIfAllPlayersHaveLeft() - Other players did not join in time, now submit your score");
        OnAllPlayersLeftRoom?.Invoke();
        yield return null;
    }

    // TODO : What if we cant connect at all.
    void OnFailedToConnectToPhoton(DisconnectCause cause)
    {

    }
    #endregion
}

public enum PhotonConnectionStatus
{
    Disconnected,
    Connected,    
    ReadyToPlay
}
