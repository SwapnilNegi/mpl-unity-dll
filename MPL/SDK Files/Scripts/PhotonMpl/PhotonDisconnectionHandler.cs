﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;


public class PhotonDisconnectionHandler : MonoBehaviourPunCallbacks
{
    public bool reconnect = false;
    private bool isReconnecting = false;

    public delegate void OnConnectionRetryOver();
    public static OnConnectionRetryOver OnReconnectionFailed;

    public static PhotonDisconnectionHandler Instance;

    public int ConnectionRetryTimeout
    {
        get
        {
            return 10;
        }
    }

    int _backgroundRetryTimeout = 10;
    public int BackgroundRetryTimeout
    {
        get
        {
            return _backgroundRetryTimeout;
        }
        set
        {
            _backgroundRetryTimeout = value;
        }
    }

    private int maxRetryCount = 2;
    public int RetryCount { set { maxRetryCount = value; } }

    #region Unity Callbacks
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != this)
            Destroy(this.gameObject);
    }

    //private void Update()
    //{
    //    if (reconnect)
    //    {
    //        reconnect = false;
    //        ReconnectToGame();
    //    }
    //}
    #endregion

    #region Photon events
    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.LogFormat("<color=red>DisconnectionHandler: Disconnection cause: {0}</color>", cause);

        if (!isReconnecting && cause != DisconnectCause.DisconnectByClientLogic)
            ReconnectToGame();
    }

    public override void OnJoinedRoom()
    {
        isReconnecting = false;
    }
    #endregion

    #region Class methods
    Coroutine reconnectCoroutine = null;
    float connectionRetryTimeout = 10;

    /// <summary>
    /// Coroutine which runs if disconnection happens due to network issues. Will not run if disconenction is from client logic.
    /// </summary>
    /// <returns></returns>
    IEnumerator ReconnectCoroutine()
    {
        connectionRetryTimeout = 10;
        isReconnecting = true;
        //Try to reconnect till the timeout is reached.
        while (connectionRetryTimeout >= 0 && !Reconnect())
        {
            Debug.Log("<color=red> PhotonDisconnectionHandler: Trying to reconnect</color> + TimeLeft: " + connectionRetryTimeout);
            yield return new WaitForSeconds(1f);
            connectionRetryTimeout -= 1;
        }

        isReconnecting = false;

        if (connectionRetryTimeout <= 0)
        {
            Debug.Log("<color=red> PhotonDisconnectionHandler: Not able to reconnect. Submit Results now</color>");
            OnReconnectionFailed?.Invoke();
            SmartFoxManager.Instance.Disconnect(MPLGameEndReason.GameEndReasons.GRECONNECTION_MAX_TIME_FINISHED);
        }
    }

    /// <summary>
    /// Returns true if connection is re-established otherwise false.
    /// </summary>
    /// <returns></returns>
    bool Reconnect()
    {
        bool isReconnected = false;

        if (PhotonNetwork.IsConnected)
        {
            isReconnected = TryToRejoinRoom();
        }
        else
        {
            Debug.LogFormat("<color=green>MPL SDK : ReconnectAndRejoin()</color>");
            if (PhotonNetwork.ReconnectAndRejoin())
            {
                if (PhotonNetwork.CurrentRoom != null)
                {
                    isReconnected = true;
                    Debug.LogFormat("<color=green>Connection re-established: roomID {0}, playerCount = {1}</color>", PhotonNetwork.CurrentRoom.Name, PhotonNetwork.CurrentRoom.PlayerCount);
                }
            }
        }
        return isReconnected;
    }

    void StopReconnectCoroutine()
    {
        //IsReconnectCalled = false;
        if (reconnectCoroutine != null)
        {
            StopCoroutine(reconnectCoroutine);
        }
        reconnectCoroutine = null;
    }

    string roomID;
    bool TryToRejoinRoom()
    {
        Debug.LogFormat("<color=green>MPL SDK : TryToRejoinRoom: roomID {0}</color>", PhotonManager.Instance.CurrentRoom);
        bool isReconnected = false;
        if (PhotonNetwork.IsConnected && PhotonNetwork.CurrentRoom != null)
        {
            StopReconnectCoroutine();
            return true;

        }

        if (PhotonNetwork.ReconnectAndRejoin())
        {
            Debug.LogFormat("<color=green>MPL SDK : Connection re-established: ReconnectAndRejoin</color>");

            isReconnected = true;
        }
        else if (PhotonNetwork.RejoinRoom(PhotonManager.Instance.CurrentRoom))
        {
            if (PhotonNetwork.CurrentRoom != null)
            {
                Debug.LogFormat("<color=green>MPL SDK :Connection re-established: roomID {0}, playerCount = {1}</color>", PhotonNetwork.CurrentRoom.Name, PhotonNetwork.CurrentRoom.PlayerCount);
                isReconnected = true;
            }
            else
            {
                Debug.LogFormat("TryToRejoinRoom: RejoinRoom FAILED");
            }
        }
        else
        {
            Debug.LogFormat("TryToRejoinRoom: ReconnectAndRejoin FAILED");
        }
        return isReconnected;
    }

    /// <summary>
    /// Tries to reconnect to the photon room.
    /// </summary>
    void ReconnectToGame()
    {
        Debug.Log("<color=green>MPL SDK : PhotonDisconnectionHandler :: ReconnectToGame()");

        if (maxRetryCount >= 1)
        {
            maxRetryCount--;
            Debug.Log("<color=green>MPL SDK : Trying to connect to Photon, Attempts left :" + maxRetryCount);
            StopReconnectCoroutine();
            reconnectCoroutine = StartCoroutine(ReconnectCoroutine());
        }
        else
        {
            Debug.Log("<color=green>MPL SDK : Photon retry count : {0} exceeded. Now Submit results" + maxRetryCount);
            OnReconnectionFailed?.Invoke();
            SmartFoxManager.Instance.Disconnect(MPLGameEndReason.GameEndReasons.GRECONNECTION_MAX_TIME_FINISHED);
        }
    }

    void LeaveRoom()
    {
        if (PhotonNetwork.IsConnected && PhotonNetwork.InRoom)
        {
            Debug.LogFormat("Leaving Room {0}", PhotonNetwork.CurrentRoom.Name);
            PhotonNetwork.LeaveRoom();

            PhotonNetwork.Disconnect();
        }
    }

    public void DisconnectPhoton()
    {
        if (PhotonNetwork.IsConnected)
        {
            PhotonNetwork.Disconnect();
        }
    }
    #endregion
}
