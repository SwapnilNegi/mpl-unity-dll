﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PhotonInputHandler : MonoBehaviour
{
	public static PhotonInputHandler instance;
    public bool gameStart;
	public bool myTurn;
	public float gamePlayTime;

	public PhotonPlayerDetails[] playerDetails = new PhotonPlayerDetails[2];

	public delegate void PhotonStates(bool connected);
	public event PhotonStates Connected,OpponentConnected;

    public delegate void PhotonTime(int time,string timeInFormat);
    public event PhotonTime RemainingTime;

    public delegate void Turn(bool myTurn,bool oppoTurn);
    public event Turn TurnUpdate;

    public delegate void PlayerDetails(PhotonPlayerDetails[] photonPlayers);
    public event PlayerDetails PlayerDetailsUpdate;

    public string timeRemainning;


    // Start is called before the first frame update
    void Awake()
    {
		instance = this;

	}
   

    public static PhotonInputHandler Instance
	{
        get
		{
			return instance;
		}
	}
    public void ConnectedTrigger(bool pValues)
    {
       if (Connected != null)
           Connected(pValues);
        Debug.Log("Connected...");
    }

    public void OppoConnectedTrigger(bool pValues)
    {
        if (OpponentConnected != null)
            OpponentConnected(pValues);
        Debug.Log("OpponentConnected...");
    }
    public void RemainingTimeTrigger(int time, string timeInFormat)
    {
        RemainingTime(time, timeInFormat);
        timeRemainning = timeInFormat;
    }
    public void TurnUpdateTrigger(bool myTurn, bool oppoTurn)
    {
        if(TurnUpdate != null)
        TurnUpdate(myTurn, oppoTurn);
    }

 
    public void PlayerDetailsTrigger(PhotonPlayerDetails[] photonPlayers)
    {
       // if(PlayerDetailsUpdate != null)
        PlayerDetailsUpdate(photonPlayers);
        playerDetails = photonPlayers;
    }

    public void TurnChangeTrigger(bool pValues)
	{

        Debug.LogError("Ravi Test Log Error :"+ pValues);
        TurnUpdateTrigger(playerDetails[0].controlTurn, playerDetails[1].controlTurn);
    }


    public void ForceTurnChange()
    {
        if(PhotonConnected() && OppoPhotonConnected() && playerDetails[0].controlTurn)
        ReconnectionHandler.Instance.OwnTurnSentEvent();
    }

    public bool PhotonConnected()
    {
        return ReconnectionHandler.Instance.PhotonConnected();
    }
    public bool OppoPhotonConnected()
    {
        return ReconnectionHandler.Instance.OppoPhotonConnected();
    }

  

}
