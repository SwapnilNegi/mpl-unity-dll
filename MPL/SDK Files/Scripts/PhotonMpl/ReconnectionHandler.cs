﻿// ----------------------------------------------------------------------------
// <copyright MPL">
// </copyright>
// <summary>
// Reconnection Handler is the central class of the MPL PUN package.
// </summary>
// <author> ravi@mplgaming.com </author>
// ----------------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;

using UnityEngine.UI;


public enum DisconnectionType
{
    Default,
    EnablePopUp
};
// For Ui Allignment
// Left to Right Or RightToLeft TODO  Done for PT  TODo
public enum UIAllignment
{
    LeftAllignOwn,
    AllignLeftRightOwnOppo
}
public delegate void TaskCompletedCallBack(string taskResult);
[System.Serializable]
public class PhotonPlayerDetails
{
    // Player Start Run Time
    [HideInInspector]
    public float playerTurnStartTime;

    // Player Turn GamePlay Time
    [HideInInspector]
    public float playerGamePlayTime;

    // Player Score
    [HideInInspector]
    public int playerScore;

    // Player Health
    [SerializeField]
    public float healthPlayer = 100;

    // Our Turn
    [SerializeField]
    public bool turn;

    // Our Turn Enable
    [SerializeField]
    public bool controlTurn;

    // Player Time Remainning
    [SerializeField]
    public float playerTimeRemainning;

  

    // Player Name
    [SerializeField]
    public string name;

    // Player Time.
    public float playerTime;

}

public class ReconnectionHandler : MonoBehaviourPunCallbacks
{

    // Photon Iniatilize Time
    float photonInitTime;

    // GamePlay Time
    float gamePlayTime;

    public int remainingTime;

    bool gameStart = false;
    public bool myTurn = false;

    [SerializeField]
    private float turnChangeTime = 15, switchDelayTime = 4;

    [HideInInspector]
    private float switchTimeInterval = 15;

    [SerializeField]
    float gamePlayDuration = 90;

    public UIAllignment allignment;

    public static ReconnectionHandler instance;

    bool master = false;


    // UI for Self Photon Connection 
    [SerializeField]
    public GameObject selfConnecting;

    // UI for Self Opponent Connection 
    [SerializeField]
    public GameObject oppoConnecting;


    public int keys;

    [SerializeField] PhotonPlayerDetails[] playerTempDetails;

    public float photonTime;

    public bool photonInit = false;

    private bool internetPopUp = false;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;

    }


    /// <summary>
    /// Photon network time, synched with the server.
    /// </summary>
    /// <remarks>
    /// v1.55<br/>
    /// This time value depends on the server's Environment.TickCount. It is different per server
    /// but inside a Room, all clients should have the same value (Rooms are on one server only).<br/>
    /// This is not a DateTime!<br/>
    ///
    /// Use this value with care: <br/>
    /// It can start with any positive value.<br/>
    /// It will "wrap around" from 4294967.295 to 0!
    /// </remarks>
    public static float PhotonMplCustomTime
    {
        get
        {
            if (UnityEngine.Time.frameCount == frame)
            {
                return frametime;
            }

            uint u = (uint)ServerTimestamp;
            float t = u;
            frametime = t / 1000.0f;
            frame = UnityEngine.Time.frameCount;
            return frametime;
        }
    }


 
    public DisconnectionType disconnectionType;

    public DisconnectionType DisconnectionPopUp
    {
        set
        {
            disconnectionType = value;
        }
    }
    public int SetGamePlayTime
    {
        set
        {
            gamePlayDuration = value;
        }
        
    }


    public int TurnTime
    {
        set
        {
            turnChangeTime = value;
        }
    }


    public int SwitchDelayTime
    {
        set
        {
            switchDelayTime = value;
        }
    }

    public void PhotonInit()
    {
        photonInit = true;
    }

    private static float frametime;
    private static int frame;

    /// <summary>
    /// The current server's millisecond timestamp.
    /// </summary>
    /// <remarks>
    /// This can be useful to sync actions and events on all clients in one room.
    /// The timestamp is based on the server's Environment.TickCount.
    ///
    /// It will overflow from a positive to a negative value every so often, so
    /// be careful to use only time-differences to check the Time delta when things
    /// happen.
    ///
    /// This is the basis for PhotonNetwork.Time.
    /// </remarks>
    public static int ServerTimestamp
    {
        get
        {
            
            return PhotonNetwork.NetworkingClient.LoadBalancingPeer.ServerTimeInMilliSeconds;   // TODO: implement ServerTimeInMilliSeconds in LBC
        }


    }

    /// <summary>The LoadBalancingClient is part of Photon Realtime and wraps up multiple servers and states for PUN.</summary>
    public static LoadBalancingClient NetworkingClient;


    /// <summary>
    /// Init 1st Time .
    /// </summary>
    ///

    private void Init()
    {
        photonInitTime = PhotonMplCustomTime;
        master = PhotonNetwork.IsMasterClient;

        // replace by Smartfox or Photon id TODO
        keys = Random.Range(0, 1000000);
        switchTimeInterval = turnChangeTime;
    }


    public static ReconnectionHandler Instance
    {
        get
        {
            return instance;
        }
    }

   
    /// <summary>
    /// Will run code this code in Per Frame to Handle Reconnection States 
    /// </summary>
    ///

    
    // Update is called once per frame
    void Update()
    {
        if (photonInitTime == 0)
            photonInitTime = PhotonMplCustomTime;

        if (!gameStart || !photonInit)
            return;

        if (PhotonMplCustomTime != 0 && PhotonNetwork.IsConnected)
            photonTime = PhotonMplCustomTime;
        else
            photonTime += Time.deltaTime;

        var index = 0;  ///0 for my Turn .. 1 for Opponent Turn
        index = (myTurn) ? 0 : 1;
        PlayerTurnUpdate(index);

        UpdateUI();

    }

    /// <summary>
    /// Turn Handle Logic with Reconnection Logic 
    /// </summary>
    ///

    float currentGamePlayTime;
    public void PlayerTurnUpdate(int turnIndex)
    {
      
        currentGamePlayTime = playerTempDetails[turnIndex].playerGamePlayTime;
        playerTempDetails[turnIndex].playerGamePlayTime = (photonTime - playerTempDetails[turnIndex].playerTurnStartTime);
        if (playerTempDetails[turnIndex].playerGamePlayTime >= switchTimeInterval)
        {
        
            TurnCotrolDisableTrigger(false);
            var turnSwitchDelay = switchTimeInterval + switchDelayTime;
            if (playerTempDetails[turnIndex].playerGamePlayTime >= turnSwitchDelay)
            {
                var resetTimeValues = playerTempDetails[turnIndex].playerGamePlayTime - switchTimeInterval - switchDelayTime;
                if (myTurn)
                    OwnTurnCompleteEvent(0, resetTimeValues);
                else
                    OppoTurnCompeleteEvent(resetTimeValues, 0);
                switchTimeInterval = turnChangeTime;
            }


        }


        gamePlayTime = photonTime - photonInitTime;
        remainingTime = (int)(gamePlayDuration - gamePlayTime);
    }



    public void TurnCotrolDisableTrigger(bool pValues)
    {
        foreach(PhotonPlayerDetails details in playerTempDetails)
        {
            details.controlTurn = pValues;
        }
 
    }


    /// <summary>
    /// Time in Min : Sec Formats 
    /// </summary>
    ///

    public string TimeString()
    {
        var min = remainingTime / 60;
        var sec = remainingTime % 60;
        var secString = (sec < 10) ? "0" + sec : sec.ToString();
        var time = "0" + min + ":" + secString;
        return time;
    }

    /// <summary>
    /// UpdateUI
    /// </summary>
    ///

    void UpdateUI()
    {

        foreach(PhotonPlayerDetails details in playerTempDetails)
        {
            details.playerTimeRemainning = turnChangeTime - details.playerGamePlayTime;
        }
        PhotonInputHandler.Instance.PlayerDetailsTrigger(playerTempDetails);
        PhotonInputHandler.Instance.RemainingTimeTrigger(remainingTime, TimeString());
    }


  




    /// <summary>
    /// Start Event 
    /// </summary>
    ///


    private IEnumerator coroutine;
    void StartGame()
    {
        Init();
        Debug.Log("Join Group....");
        photonInitTime = PhotonMplCustomTime;
        gameStart = true;

        if (master)
        {
            myTurn = true;
            TurnChange(myTurn);
            PhotonInputHandler.Instance.TurnChangeTrigger(myTurn);
        }

        foreach (PhotonPlayerDetails player in playerTempDetails)
        {
            player.playerTurnStartTime = PhotonMplCustomTime;
            player.playerGamePlayTime = 0;
        }

        // Player Name RoomHandler.Instance.roomName = PhotonNetwork.CurrentRoom.Name;

        var internetCheckInterval = 0.2f;
        coroutine = CheckInternetConnection(internetCheckInterval);
        StartCoroutine(coroutine);
    }



    /// <summary>
    /// Reset Event 
    /// </summary>
    ///

    void Reset(float[] playerTime)
    {

        myTurn = !myTurn;
        TurnChange(myTurn);
        PhotonInputHandler.Instance.TurnChangeTrigger(myTurn);

        var count = 0;
        foreach (PhotonPlayerDetails player in playerTempDetails)
        {
            playerTempDetails[count].playerTurnStartTime = PhotonMplCustomTime - playerTime[count];
            player.playerGamePlayTime = 0;
            count++;
        }

    }

    /// <summary>
    /// TurnChange Event 
    /// </summary>
    ///

    public void TurnChange(bool pValues)
    {

        foreach (PhotonPlayerDetails player in playerTempDetails)
        {
            player.turn = pValues;
            player.controlTurn = pValues;
            pValues = !pValues;
        }
 
  
    }
   
 
  
    public void SendFromUserOwn()
    {
        ReceiveEventHandler.Instance.SendByOwnEvent(gamePlayTime);
    }
    public void CallBackRecieveUserOppo(float time)
    {
        SendFromUserOppo(time);
    }
    public void SendFromUserOppo(float time)
    {
        ReceiveEventHandler.Instance.SendByOpoEvent(time);
    }

    public void CallBackRecieveFromOwn(float time)
    {
       // PhotonInputHandler.Instance.OppoConnectedTrigger();
    }



    /// <summary>
    /// Change Turn Own Event
    /// </summary>
    ///

    void OwnTurnCompleteEvent(float pPlayerTime, float pOppoTime)
    {
        if (myTurn)
        {
            var time = new float[2] { pPlayerTime, pOppoTime };
            Reset(time);

        }

    }

    /// <summary>
    /// Change Turn Opponent Event
    /// </summary>
    ///

    void OppoTurnCompeleteEvent(float pPlayerTime, float pOppoTime)
    {
        if (!myTurn)
        {
            var time = new float[2] { pPlayerTime, pOppoTime };
            Reset(time);

        }

    }


    /// <summary>
    /// Change Turn before Time End Event
    /// </summary>

    public void OwnTurnSentEvent()
    {
        ReceiveEventHandler.Instance.OwnTurnSentEvent();
    }


    /// <summary>
    /// Change Turn before Time End Callback
    /// </summary>

    public void OwnTurnReceiverEvent(float pTime)
    {
        switchTimeInterval = currentGamePlayTime;
    }

    /// <summary>
    /// Change Opponent Turn before Time End Event
    /// </summary>
    ///<remarks>
    /// Only for Testing...... Need to Remove TODO TODO
    /// TODO
    /// </remarks>

    public void OppoTurnSentEvent()
    {
        ReceiveEventHandler.Instance.OppoTurnSentEvent();
    }


    /// <summary>
    /// Change Opponent Turn before Time End Callback
    /// </summary>
    ///<remarks>
    /// Only for Testing...... Need to Remove TODO TODO
    /// TODO
    /// </remarks>

    public void OppoTurnReceiverEvent(float pTime)
    {
        switchTimeInterval = currentGamePlayTime;

    }



    /// <summary>
    /// Update Score Event
    /// </summary>

    public void SendScore(int score)
    {
        ReceiveEventHandler.Instance.ScoreUpdate(score, keys);
    }

    /// <summary>
    /// Update Score CallBack
    /// </summary>
    ///

    public void ScoreUpdate(int pValues, int pKeys)
    {
        var indexCount = 0;
        indexCount = (keys == pKeys) ? 0 : 1;
        playerTempDetails[indexCount].playerScore += pValues;

    }


    /// <summary>
    /// Deduct Own Health
    /// </summary>

    public void DeductOwnHealth(int health)
    {
        ReceiveEventHandler.Instance.DeductScoreOwn(health, keys);
    }


    /// <summary>
    /// Deduct Opponent Health
    /// </summary>
    /// 
    public void DeductOppoHealth(int health)
    {
        ReceiveEventHandler.Instance.DeductScoreOppo(health, keys);
    }


    /// <summary>
    /// Response Update Own and  Opponent Health
    /// </summary>
    /// 
    public void ScoreHealthUpdateResponse(float health1, float health2, int pKeys)
    {

        var health = (keys == pKeys) ? new float[2] { health1, health2 } : new float[2] { health2, health1 };

        var count = 0;
        foreach (PhotonPlayerDetails player in playerTempDetails)
        {
            player.healthPlayer = health[count];
            count++;

        }

    }



    /// <summary>
    /// Send Data from 1 to 1 to check Own Internet
    /// </summary>
    /// <remarks>
    /// Send Data from 1 to 2 to check Opponent Internet
    /// happen.
    /// </remarks>
    ///

    public void PingToAll()
    {
        if (master)
            ReceiveEventHandler.Instance.SendPingDataMaster(keys);
        else
            ReceiveEventHandler.Instance.SendPingDataMOther(keys);
    }

    float ownInternetTime;
    float oppInternetTime;

    /// <summary>
    /// Recieve Ping from 1 to 1 to check Own Internet  
    /// </summary>
    /// <remarks>
    /// Recieve Ping from 1 to 2 to check Opponent Internet 
    /// happen.
    /// </remarks>
    ///

    public void RecievePing(int pValues)
    {

        if (pValues == keys)
        {
            // Own Internet
            ownInternetTime = Time.time;
        }
        else
        {
            // Oppo Internet
            oppInternetTime = Time.time;
        }
    }







    /// <summary>
    /// Check Photon Self Connection in ideal time 1.5 Secs
    /// </summary>
    /// <remarks>
    /// Check Photon Oppon Connection in ideal time 1.5 Secs
    /// happen.
    /// </remarks>
    ///

    bool ownPhotonPingConnected = true;
    bool oppoPhotonPingConnected = true;

    IEnumerator CheckInternetConnection(float waitTime)
    {
        var internetReadCounter = 1.5f;
        while (true)
        {

            yield return new WaitForSeconds(waitTime);
            PingToAll();

            ownPhotonPingConnected = ((Time.time - ownInternetTime) >= internetReadCounter) ? false : true;
        

            oppoPhotonPingConnected = ((Time.time - oppInternetTime) >= internetReadCounter) ? false : true;
            OpponentConnection();
     

        }

    }

    private void OnDestroy()
    {
        if(coroutine != null)
        StopCoroutine(coroutine);
    }


    /// <summary>
    /// Check Photon Self Connection in case by OnDisconnected event
    /// </summary>

    bool connected = true;
    bool oppoConnected = true;

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("Disconnected from photon" + cause);
        connected = false;
        if(runnningStatus)
        {
            OnConnected(true);
        }
       

    }

  

    private void OnConnected(bool pValues)
    {
        if(disconnectionType == DisconnectionType.EnablePopUp)
        selfConnecting.SetActive(pValues);
        PhotonInputHandler.Instance.ConnectedTrigger(!pValues);
    }

  
    private void OppoOnConnected(bool pValues)
    {
        if (disconnectionType == DisconnectionType.EnablePopUp)
            oppoConnecting.SetActive(pValues);
        PhotonInputHandler.Instance.OppoConnectedTrigger(!pValues);
    }

    /// <summary>
    /// OnJoinedRoom Event 
    /// </summary>
    ///

    bool firstLoad = false;
    public bool isMaster = false;
    public override void OnJoinedRoom()
    {
        if (PhotonNetwork.PlayerList.Length == 2 && !gameStart)
        StartGame();
        connected = true;
        OnConnected(false);
        OppoOnConnected(false);
        if (!firstLoad)
        {
            firstLoad = true;
            isMaster = PhotonNetwork.IsMasterClient;
        }

        if (isMaster && !PhotonNetwork.IsMasterClient)
        PhotonNetwork.SetMasterClient(PhotonNetwork.PlayerList[0]);

       
    }



    /// <summary>
    /// Check Photon Self Connection in case  OnConnected event
    /// </summary>
    public override void OnConnected()
    {

        Debug.Log("Connected ....." + connected);
        connected = true;
    }



    public bool PhotonConnected()
    {
        // return (connected && ownPhotonPingConnected);

        return connected;
    }

    public bool OppoPhotonConnected()
    {
        // return (oppoConnected && oppoPhotonPingConnected);

        return (oppoConnected  && oppoPhotonPingConnected);
    }



    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Debug.Log("Ravi Ranjan Kumar...." + otherPlayer.UserId);
        oppoConnected = false;
        OpponentConnection();
    }
    public bool runnningStatus = false;
    void OpponentConnection()
    {
        var opponentConnected = (oppoConnected && oppoPhotonPingConnected) ? true : false;
        if(runnningStatus)
            OppoOnConnected(!opponentConnected);
        else
            OppoOnConnected(false);
    }

    /// <summary>
    /// PlayerEnteredRoom Event 
    /// </summary>
    ///

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        if (PhotonNetwork.PlayerList.Length == 2 && !gameStart)
            StartGame();

        Debug.Log("OnPlayerEnteredRoom ....." + connected);

        oppoConnected = true;
        OpponentConnection();
    }



    bool playing = true;
    public void Restart()
    {
       
        ownInternetTime = Time.time;
        oppInternetTime = Time.time;
        oppoPhotonPingConnected = true;
        connected = true;
        OnConnected(false);
        oppoConnected = true;
        OppoOnConnected(false);
        pauseConnected = true;
        playing = true;
        OpponentConnection();
    }

    private void OnApplicationPause(bool pause)
    {
        Debug.LogError("Pause Delay Call: " + pause);
        ReceiveEventHandler.Instance.SendPauseEvent(pause);
    }

    bool pauseConnected = true;
    public void PauseEventCallBack(bool pStates)
    {
        pauseConnected = !pStates;
        OpponentConnection();
        Debug.LogError("Pause Delay CallBack....: " + pStates);
    }

    public void DisconnectedbyCleintLogic()
    {
        playing = false;
        OpponentConnection();
    }
 
}
