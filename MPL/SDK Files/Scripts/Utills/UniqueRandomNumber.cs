﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

#region Serialized & Instance related methods & variables. (Accessabel from MPLController.instance.uniqueRandomNumber only)
public partial class UniqueRandomNumber
{

    static Dictionary<string, List<int>> randomNumberLists = new Dictionary<string, List<int>>();
    static int RandomHash = -1;

    public void ClearHash()
    {
        randomNumberLists.Clear();
    }

    public void GenerateRandomHash(string battleID_S)
    {
        int ans = 1;

        byte[] asciiV = Encoding.ASCII.GetBytes(battleID_S);
        try
        {
            for (int i = 0; i < asciiV.Length; i++)
            {
                ans += asciiV[i];
            }
        }
        catch (Exception ex)
        {
            Debug.LogError("Error in creating Battle-Id-Hash: " + ex.Message);
        }

        RandomHash = ans;
    }

    static int GetRandomHash()
    {
        if (RandomHash == -1)
        {
            RandomHash = new System.Random().Next(1, 9999);
        }

        return RandomHash;
    }
}
#endregion


#region Static Methods
public partial class UniqueRandomNumber
{

    /// <summary>
    /// NOT RECOMMENDED: Use this if you want random number from multiple times. But this is expensive method.
    /// </summary>
    /// <param name="_min">Define min of your input</param>
    /// <param name="_max">Define max of your input</param>
    /// <param name="_iteration">Current iteration time</param>
    /// <returns>Random Number</returns>
    public static int GetRandomNumber(int _min, int _max, int _iteration)
    {
        //Error validations
        if (_min == _max)
        {
            return _min;
        }
        return   _min + GenerateRandomNumber_((_max - _min), _iteration);
        
    }

    /// <summary>
    /// RECOMMENDED: Use this if you need Random numbers multiple times from your list & Input size is more than 1K.
    /// This is Optimized function. Downside is: You have to return the hash in ref everytime.
    /// </summary>
    /// <param name="_min">Define min of your input</param>
    /// <param name="_max">Define max of your input</param>
    /// <param name="_iteration">Current iteration time</param>
    /// <param name="_hashID">_hashID received</param>
    /// <returns>Random Number</returns>
    public static int GetRandomNumber_Faster(int _min, int _max, int _iteration, ref string _hashID)
    {
        //Error validations
        if (_min == _max)
        {
            return _min;
        }
        return _min + GenerateRandomNumber_((_max - _min), _iteration, ref _hashID);
    }

    /// <summary>
    /// HIGHLY RECOMMENDED: Get a complete list of random numbers from your input
    /// </summary>
    /// <param name="_min">Define min of your input</param>
    /// <param name="_max">Define max of your input</param>
    /// <param name="_outputLength">How many numbers in you need randomly</param>
    /// <returns>Array of Random Numbers</returns>
    public static int[] GetRandomNumberSequence(int _min, int _max, int _outputLength)
    {
        //Error validations
        if (_outputLength == 0)
        {
            Debug.LogError("UniqueRandomNumber input error in GenerateRandomNumberSequence: _outputLength received as 0 (zero), it should be minimum 1");
            return null;
        }
        if (_min == _max)
        {
            return new int[] { _min};
        }
        

        int[] ans = GenerateRandomNumberSeq((_max - _min), _outputLength);
        for(int i=0; i<ans.Length; i++)
        {
            ans[i] += _min;
        }
        return ans;
    }
}
#endregion

#region private functionalities
public partial class UniqueRandomNumber
{
    //This is expensive for high number of iterations
    static int GenerateRandomNumber_(int _length, int _iteration)
    {
        #region Initializations
        int _initIteration = _iteration;
        int RoomID = GetRandomHash();

        if (RoomID == 0)
            RoomID = 1;

        int startSeed = RoomID % _length;

        if (_iteration > _length)
        {
            int crossOver = (_iteration - 1) / _length;
            _iteration = ((_iteration - 1) % _length) + 1;
            startSeed = (startSeed + crossOver) % _length;

        }


        int ans = 0;
        int zone = 0;
        int iterationStartIndex = -1;
        int currentIndex = -1;
        int increment = ((startSeed + (RoomID / _length)) % _length) + 1;

        List<int> elements = new List<int>();

        for (int i = 0; i < _length; i++)
        {
            elements.Add(i);
        }

        int totalIteration = _iteration;
        int currentIteration = 1;
        #endregion

        #region Algo
        while (totalIteration > 0)
        {
            zone = zone == 3 ? 0 : zone;

            iterationStartIndex = (((elements.Count / 3) * zone) - 1 + (currentIteration % 2 == 0 ? startSeed : 0)) % _length;
            currentIndex = iterationStartIndex + increment;
            currentIndex = currentIndex % elements.Count;

            if (totalIteration == 1)
            {
                return elements[currentIndex];
            }
            else
                elements.RemoveAt(currentIndex);

            zone++;
            currentIteration++;
            totalIteration--;
        }
        #endregion

        return ans;
    }

    //This is Optimized for high numberif iterations
    static int GenerateRandomNumber_(int _length, int _iteration, ref string _hashID)
    {
        #region Error-Validations
        if (string.IsNullOrEmpty(_hashID))
        {
            _hashID = " ";
        }
        #endregion

        #region Initializations
        int RoomID = GetRandomHash();

        if (RoomID == 0)
            RoomID = 1;

        int startSeed = RoomID % _length;
        if (_iteration > _length)
        {
            int crossOver = (_iteration - 1) / _length;
            _iteration = ((_iteration - 1) % _length) + 1;
            startSeed = (startSeed + crossOver) % _length;

        }


        int ans;
        int zone;
        int iterationStartIndex;
        int currentIndex = -1;
        int increment = ((startSeed + (RoomID / _length)) % _length) + 1;

        List<int> elements = new List<int>();

        if (!randomNumberLists.ContainsKey(_hashID) || randomNumberLists[_hashID].Count == 0)
        {
            for (int i = 0; i < _length; i++)
            {
                elements.Add(i);
            }
        }
        else
        {
            elements = randomNumberLists[_hashID];
        }
        #endregion

        #region Algo

        zone = ((_iteration - 1) % 3);

        iterationStartIndex = (((elements.Count / 3) * zone) - 1 + (_iteration % 2 == 0 ? startSeed : 0)) % _length;

        currentIndex = iterationStartIndex + increment;
        currentIndex = currentIndex % elements.Count;

        ans = elements[currentIndex];
        elements.RemoveAt(currentIndex);

        if (!randomNumberLists.ContainsKey(_hashID))
        {
            _hashID = (1000000 + randomNumberLists.Count).ToString();
        }
        randomNumberLists[_hashID] = elements;
        #endregion

        return ans;


    }

    //This is recommended as it returns the whole list at once
    static int[] GenerateRandomNumberSeq(int _length, int _iteration)
    {

        #region Initializations
        int RoomID = GetRandomHash();
        int[] allNums = new int[_iteration];

        if (RoomID == 0)
            RoomID = 1;

        int total = _iteration;
        int totalCache = total;
        _iteration = Mathf.Min(_length, _iteration);
        int _iterationCache = _iteration;
        int ans;
        int zone = 0;
        int iterationStartIndex = -1;
        int currentIndex = -1;
        int startSeed = RoomID % _length;
        int increment = ((startSeed + (RoomID / _length)) % _length) + 1;
        int num;

        List<int> elements = new List<int>();

        for (int i = 0; i < _length; i++)
        {
            elements.Add(i);
        }
        #endregion

        #region Algo
        while (total > 0)
        {
            if (elements.Count == 0)
            {
                zone = 0;
                iterationStartIndex = -1;
                startSeed = RoomID % _length;
                currentIndex = -1;
                for (int i = 0; i < _length; i++)
                {
                    elements.Add(i);
                }

                int crossOver = (total - 1) / _length + 1;

                _iteration = ((_iterationCache - 1) % _length) + 1;
                startSeed = (startSeed + crossOver) % _length;
                increment = ((startSeed + (RoomID / _length)) % _length) + 1;

            }
            zone = zone == 3 ? 0 : zone;
            iterationStartIndex = (((elements.Count / 3) * zone) - 1 + (_iteration % 2 == 0 ? startSeed : 0)) % _length;
            currentIndex = iterationStartIndex + increment;
            currentIndex = currentIndex % elements.Count;

            if (total == 1)
            {
                allNums[totalCache - total] = elements[currentIndex];
                return allNums;
            }
            else
            {
                num = elements[currentIndex];
                allNums[totalCache - total] = num;
                elements.RemoveAt(currentIndex);
            }

            zone++;
            _iteration--;
            total--;
        }
        #endregion

        return null;
    }

}

#endregion

